//
//  ThirdViewController.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ThirdViewController.h"
#import "NSTimer+BlockTimer.h"

@interface ThirdViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation ThirdViewController
{
    NSInteger i;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"方法三 NSTimer分类 解决timer的强引用";

    __weak typeof(self) weakSelf = self;
    [NSTimer tst_scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [weakSelf run];
    }];
    i = 0;
        
}

- (void)run {
    NSLog(@"解除NSTimer强引用方法三 -- %ld", ++i);
}

- (void)dealloc
{
    [self.timer invalidate];
    NSLog(@"%@ %@", self, NSStringFromSelector(_cmd));
}

@end

//
//  SecondViewController.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation SecondViewController
{
    NSInteger i;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"方法二 Block & weakSelf解决timer的强引用";
       
    __weak typeof(self) weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [weakSelf run];
    }];
    i = 0;
}

- (void)run {
    NSLog(@"解除NSTimer强引用方法二 -- %ld", ++i);
}

- (void)dealloc
{
    [self.timer invalidate];
    NSLog(@"%@ %@", self, NSStringFromSelector(_cmd));
}

@end

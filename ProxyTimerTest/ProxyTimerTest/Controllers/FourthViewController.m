//
//  FourthViewController.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "FourthViewController.h"

@interface FourthViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation FourthViewController
{
    NSInteger i;
    int flag;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"方法四 子线程解决NSTimer的强引用";
    i = 0;
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self startTimer];
    });
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"%s", __FUNCTION__);

    flag = 1;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    NSLog(@"%s", __FUNCTION__);
}

- (void)startTimer {
    NSLog(@"%@", [NSThread currentThread]);

    flag = 0;
    self.timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(run) userInfo:nil repeats:YES];
    [NSRunLoop.currentRunLoop addTimer:self.timer forMode:NSDefaultRunLoopMode];
    [NSRunLoop.currentRunLoop run];
//    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(run) userInfo:nil repeats:YES];
//    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//    NSLog(@"startTimer pass");
}

- (void)run {
    if (flag == 1) {
        [self.timer invalidate];
        self.timer = nil;
        
        NSLog(@"已经释放 NSTimer");
    }
    NSLog(@"解除NSTimer强引用方法四 -- %ld", ++i);
}

- (void)dealloc
{
//    CFRunLoopStop([[NSRunLoop currentRunLoop] getCFRunLoop]);
    NSLog(@"已经释放 runLoop");
    NSLog(@"%@ %@", self, NSStringFromSelector(_cmd));
}

@end

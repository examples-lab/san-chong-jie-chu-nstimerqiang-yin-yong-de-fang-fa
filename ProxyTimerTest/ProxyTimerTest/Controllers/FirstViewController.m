//
//  FirstViewController.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "FirstViewController.h"
#import "TimerProxy.h"

@interface FirstViewController ()
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation FirstViewController
{
    NSInteger i;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"方法一 NSProxy解决timer的强引用";
        
    TimerProxy *proxy = [TimerProxy alloc];
    proxy.obj = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:proxy selector:@selector(run) userInfo:nil repeats:YES];
    i = 0;
}

- (void)run {
    NSLog(@"解除NSTimer强引用方法一 -- %ld", ++i);
}

- (void)dealloc
{
    [self.timer invalidate];
    NSLog(@"%@ %@", self, NSStringFromSelector(_cmd));
}

@end

//
//  NSTimer+BlockTimer.h
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSTimer (BlockTimer)

typedef void (^TSTCallBack)(NSTimer *timer);

+ (NSTimer *)tst_scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(TSTCallBack)block;

@end

NS_ASSUME_NONNULL_END

//
//  NSTimer+BlockTimer.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "NSTimer+BlockTimer.h"

@implementation NSTimer (BlockTimer)
+ (NSTimer *)tst_scheduledTimerWithTimeInterval:(NSTimeInterval)interval repeats:(BOOL)repeats block:(TSTCallBack)block {
    return [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(tst_block:) userInfo:block repeats:repeats];
}

+ (void)tst_block:(NSTimer *)timer {
    TSTCallBack callBack = timer.userInfo;
    if (callBack) {
        callBack(timer);
    }
}
@end

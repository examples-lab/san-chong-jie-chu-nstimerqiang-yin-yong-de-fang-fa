//
//  TimerProxy.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "TimerProxy.h"

@implementation TimerProxy

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
    NSMethodSignature *sig = [self.obj methodSignatureForSelector:sel];
    
    return sig;
}

- (void)forwardInvocation:(NSInvocation *)invocation {
    [invocation invokeWithTarget:self.obj];
}

//- (void)dealloc
//{
////    i = 0;
//    NSLog(@"%@ %@",self, NSStringFromSelector(_cmd));
//}

@end

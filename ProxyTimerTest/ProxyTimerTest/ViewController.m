//
//  ViewController.m
//  ProxyTimerTest
//
//  Created by 金鑫 on 2020/5/31.
//  Copyright © 2020 金鑫. All rights reserved.
//

#import "ViewController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "FourthViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIView *hookedView = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
//    hookedView.backgroundColor = [UIColor redColor];
//    UIView *proxy = 
}

- (IBAction)jumpBtnClick1:(id)sender {
    FirstViewController *firstVc = [[FirstViewController alloc] init];
    [self.navigationController pushViewController:firstVc animated:YES];
}

- (IBAction)jumpBtnClick2:(id)sender {
    SecondViewController *secondVc = [[SecondViewController alloc] init];
    [self.navigationController pushViewController:secondVc animated:YES];
}

- (IBAction)jumpBtnClick3:(id)sender {
    ThirdViewController *thirdVc = [[ThirdViewController alloc] init];
       [self.navigationController pushViewController:thirdVc animated:YES];
}

- (IBAction)jumpBtnClick4:(id)sender {
    FourthViewController *thirdVc = [[FourthViewController alloc] init];
    [self.navigationController pushViewController:thirdVc animated:YES];
}

@end
